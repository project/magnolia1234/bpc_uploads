==================================================
Filename          : bypass-paywalls-chrome-clean-4.0.0.0-kiwi-custom.crx
SHA-256           : 15ce0b3d360f2605a23eac7a43607e8d38c266becdf5b09163b6da5f2d45b530
Modified Time     : 12-1-2025 21:35:39
File Size         : 216.843
Extension         : crx
==================================================

==================================================
Filename          : bypass-paywalls-chrome-clean-4.0.5.0.crx
SHA-256           : 2cd8516f8c7636b694b65aac93c8a8a4565ca22c5e7fb47e4d28e2fdbed4d135
Modified Time     : 16-2-2025 19:48:30
File Size         : 230.680
Extension         : crx
==================================================

==================================================
Filename          : bypass-paywalls-chrome-clean-4.0.6.0.crx
SHA-256           : 009a4175294b2cb689fcabf6e209e61a5ba887f7361ceb5709ec9d13aa6f3ee1
Modified Time     : 2-3-2025 19:39:05
File Size         : 232.351
Extension         : crx
==================================================

==================================================
Filename          : bypass-paywalls-chrome-clean-master.zip
SHA-256           : 85d9285810ea9cd96d9a365c2d35bf8dc5a58f63f0e674cc8637c33d27ca8dee
Modified Time     : 8-3-2025 17:47:45
File Size         : 241.710
Extension         : zip
==================================================

==================================================
Filename          : bypass-paywalls-firefox-clean-master.zip
SHA-256           : bf22c9f0cc5e9ae6a84ea883a61ece9a98685760283d642e3f6f950ad4dd936c
Modified Time     : 8-3-2025 17:47:53
File Size         : 238.589
Extension         : zip
==================================================

==================================================
Filename          : bypass_paywalls_clean-4.0.5.0-custom.xpi
SHA-256           : 82f28d85d78125c6cfb54b3d3600c99033307974e1b9bed290d2e07cd26497a9
Modified Time     : 16-2-2025 19:52:47
File Size         : 236.412
Extension         : xpi
==================================================

==================================================
Filename          : bypass_paywalls_clean-4.0.5.0.xpi
SHA-256           : bd5a3b2a52a54305ade5adf2d9f7f7cd6165f1873d76e87f8ebe130b9b1ad18e
Modified Time     : 21-2-2025 15:36:27
File Size         : 242.046
Extension         : xpi
==================================================

==================================================
Filename          : bypass_paywalls_clean-4.0.6.0-custom.xpi
SHA-256           : ddc53ebc738c693f794834b892beeb541a91104d224ff9ad75a7788665395264
Modified Time     : 2-3-2025 19:50:52
File Size         : 238.175
Extension         : xpi
==================================================

==================================================
Filename          : bypass_paywalls_clean-4.0.6.0.xpi
SHA-256           : f4e530a37d72c96b37e76279e31925911da3309bbd5bbaf41a17e4b31a7e487f
Modified Time     : 6-3-2025 11:20:24
File Size         : 243.813
Extension         : xpi
==================================================

==================================================
Filename          : bypass_paywalls_clean-latest.xpi
SHA-256           : f4e530a37d72c96b37e76279e31925911da3309bbd5bbaf41a17e4b31a7e487f
Modified Time     : 6-3-2025 11:20:24
File Size         : 243.813
Extension         : xpi
==================================================

