# Bypass Paywalls Clean

Extension allows you to read articles from (supported) sites that implement a paywall.

You can also add a domain as custom site and try to bypass the paywall.  
Weekly updates are released for fixes and new sites.

Installation instructions.

Chrome: [https://gitflic.ru/project/magnolia1234/bypass-paywalls-chrome-clean#installation](https://gitflic.ru/project/magnolia1234/bypass-paywalls-chrome-clean#installation)

Firefox: [https://gitflic.ru/project/magnolia1234/bypass-paywalls-firefox-clean#installation](https://gitflic.ru/project/magnolia1234/bypass-paywalls-firefox-clean#installation)

